<?php 
session_start(); 
// récupération et test variable
if(isset($_GET['id']) && $_GET['id'] != '' ){
    $id = $_GET['id'];
}else{
    die('Page introuvable');
}
$user = 'root'; 
$password = null;
// Gestion BDD et Requête 
try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $stmt = $dbh->prepare("SELECT id,nom,prenom FROM utilisateurs WHERE id = :id LIMIT 1");
    $stmt->bindParam(':id',$id);
    $stmt->execute(); 
    // Vérification du contenu retourné 
    $result = $stmt->fetch();
    if(!$result){
        die('erreur id introuvable'); 
    }
    
}
catch(Exception $e){
    // Gestion des exceptions 
    var_dump($e);
    die('erreur'); 
}

$_SESSION['nom'] = $result['nom'];
$_SESSION['prenom'] = $result['prenom'];
$_SESSION['id'] = $result['id'];
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- création et remplissage des valeurs -->
    <form method="POST" action="traitement_update.php">
        <label for="nom">Nom</label>
        <input type="text" name="nom" value="<?= $result['nom'];?>">
        <label for="prenom">prenom</label>
        <input type="text" name="prenom" value="<?php echo $result['prenom'];?>">
        <input type="submit">
    </form>
</body>
</html>