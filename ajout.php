<?php 

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
    <h1 class="col-12">AJOUTER UN UTILISATEUR</h1>
    <form class="row form" action="traitement.php" method="POST">
        <div class="col-6 form-action">
            <label for="nom">nom</label>
            <input class="form-control" name="nom" type="text">
        </div>
        <div class="col-6 form-action">
            <label for="prenom">prenom</label>
            <input class="form-control" name="prenom" type="text">
        </div>
        <div class="col-6 form-action">
            <label for="prenom">Annee naissance</label>
            <input class="form-control" name="annee_naissance" type="text">
        </div>
        <br>
        <input class="col-3 col-centered text-center btn btn-success" type="submit">
    </form>
    <a class="" href="/formulaire">Retourner à l'accueil</a>
    </div>
</div>
</body>
</html>