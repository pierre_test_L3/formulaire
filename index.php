<?php 
session_start(); 

require 'credentials.php'; 

try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $stmt = $dbh->query("SELECT * FROM utilisateurs");

    $resultats = $stmt->fetchAll(); 
    var_dump(count($resultats));
}
catch(Exception $e){
    var_dump($e);
}

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
    <?php 
        foreach($resultats as $resultat): ?>
        
        <tr>
            <td><?php echo $resultat['id'] ?> </td>
            <td><?php echo $resultat['nom'] ?></td>
            <td><?php echo $resultat['prenom'] ?></td>
            <td>
                <form method="POST" action="delete.php">
                    <input type="hidden" name="id" value="<?php echo $resultat['id'] ?>">
                    <input type="submit">
                </form>
            </td>
            <td>
                <a href="update.php?id=<?= $resultat['id']  ?>">Editer  </a>
            </td>
        </tr>

        <?php endforeach; 
    ?>
    </table>
    <a href="ajout.php">Ajouter un utilisateur</a>
</body>
</html>